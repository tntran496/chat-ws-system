use mychat;
ALTER DATABASE mychat CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table User(
    id varchar(255) not null,
    createdAt timestamp default current_timestamp,
    updatedAt timestamp default current_timestamp,
    picture varchar(255),
    fullName  varchar(255),
    email  varchar(255),
    primary key (id)
);
ALTER TABLE User CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table Room(
    id int not null auto_increment,
    createdAt timestamp default current_timestamp,
    updatedAt timestamp default current_timestamp,
    UserId varchar(255),
    name varchar(255),
    description varchar(500),
    image varchar(500),
    primary key (id),
    constraint fk_User_room foreign key (UserId) references User(id)
);
ALTER TABLE Room CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table RoomUser(
    UserId varchar(255),
    RoomId int,
    primary key (UserId,RoomId),
    constraint fk_User_ga foreign key (UserId) references User(id),
    constraint fk_room_ga foreign key (RoomId) references Room(id)
);
ALTER TABLE RoomUser CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table Message(
    id int not null auto_increment,
    createdAt timestamp default current_timestamp,
    updatedAt timestamp default current_timestamp,
    UserId varchar(255),
    RoomId int,
    content varchar(2048),
    primary key (id),
    constraint fk_room_message foreign key (RoomId) references Room(id),
    constraint fk_User_message foreign key (UserId) references User(id)
);
ALTER TABLE Message CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;


