//env
console.log(require('dotenv').config())
const Websocket = require('ws').Server
const morgan = require('morgan')
const express = require('express')
const cors = require('cors')
const http = require('http')
const app = express()
const server = http.createServer(app)
const passport = require('./src/middleware/passport.google')

const ChatWorker = require('./src/services/ws')

const router = require('./src/routers/main.routers')

const port = process.env.PORT || 3000
const wss = new Websocket({
    server: server,
})


//morgan
app.use(morgan('dev'))

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//passport
app.use(passport.initialize())

//cors
app.use(cors())

//app route
router(app)

const chat = new ChatWorker(wss)
chat.broker(wss)

server.listen(port, () => console.log(`Server listen on port ${port}`))

process.on('SIGTERM', () => {
    server.close(() => {
        console.log(`Server close`)
    })
})