const express = require('express')
const router = express.Router()

const middleware = require('../middleware/auth')
const RoomController = require('../controllers/room.controller')

router.post('/',middleware.auth, RoomController.createRoom)
router.get('/room-user',middleware.auth,RoomController.getRoomByUser)
router.post('/:id/add-user',middleware.auth, RoomController.addUsersInRoom)

module.exports = router

