const express = require('express')
const router = express.Router()
const passport = require('../middleware/passport.google')
const middlewares = require('../middleware/auth')

router.get("/auth/google", passport.authenticate("google", {
    scope: ["profile", "email"]
}));

router.get("/auth/google/redirect", passport.authenticate("google",{session : false}), async (req, res) => {
    console.log(req.user)
    const token = await middlewares.generateJWT({id :req.user})
    res.status(200).send({token : `Bearer ${token}`});
});

module.exports = router