const RoomRouter = require('./room.router')
const OauthRouter = require('./oauth.router')

function route(app){
    app.use('/api/room',RoomRouter)
    app.use('/',OauthRouter)
}
module.exports = route