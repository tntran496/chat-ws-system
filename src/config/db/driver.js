const {
    Sequelize
} = require('sequelize');
const sequelize = new Sequelize(
    process.env.DB_NAME || 'mychat',
    process.env.DB_USER ||'user',
    process.env.DB_PASS || '123456', 
    {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || 3306,
        dialect: 'mysql',
        pool: {
            max: 200,
            min: 0,
            idle: 10000
        },
        define: {
            charset: 'utf8',
            collate: 'utf8_general_ci',
        },

    }
);
module.exports = sequelize