const hanErr = require('../utils/error')
const Room = require('../models/room');
const RoomUser = require('../models/roomuser')

module.exports = {
    createRoom : async (req,res,next) => {
        try{
            const {user,body} = req
            console.log(user.id)
            body.UserId = user.id
            console.log(body)
            const dto = await Room.create(body)
            res.status(200).send(dto)
        }catch(err){
            hanErr(req,res,next,err)
        }
    },
    getRoomByUser : async (req,res,next) => {
        try{
            const {user} = req
            const room = await Room.findAll({
                where: {
                    UserId : user.id
                }
            })
            res.status(200).send(room)
        }catch(err){
            hanErr(req,res,next,err)
        }
    },
    addUsersInRoom : async (req,res,next) =>{
        try{
            const {body,params} = req
            const idRoom = params.id
            const listIdUsers = body.ids
            for(let id of listIdUsers){
                await RoomUser.create({RoomId : idRoom,UserId : id})
            }
            res.status(200).send({message : true})
        }catch(err){
            hanErr(req,res,next,err)
        }
    }
}