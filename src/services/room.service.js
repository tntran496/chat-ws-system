const Room = require('../models/room')
const RoomUser = require('../models/roomuser')

const RoomService = {
    getRoomIdUserJoin: async (userId) => {
        try {
            const listRoom = []
            const idsRoomJoin = await RoomUser.findAll({
                where: {
                    UserId: userId
                }
            })
            for (let id of idsRoom) {
                let room = await Room.findAll({
                    where: {
                        id: id
                    }
                })
                if (room.length > 0) {
                    listRoom.push(room)
                }
            }
            return listRoom
        } catch (err) {
            console.log(err)
        }
    },
    checkRoomExist: async (id) => {
        try {
            const room = await Room.findAll({where: {id: id}})
            if(room.length>0){
                return true
            }else{
                return false
            }
        }catch(err){
            console.log(err)
        }
    }

}

module.exports = RoomService