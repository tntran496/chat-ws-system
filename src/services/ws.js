const middleware = require('../middleware/auth')
const RoomService = require('../services/room.service')
const RoomUser = require('../models/roomuser')
const Message = require('../models/message')
const url = require('url')

class ChatBroker {
    constructor(wss) {
        this.wss = wss
    }
    broker() {
        this.wss.on('connection', async (ws, req) => {
            try {
                //get token in url ws
                const token = url.parse(req.url).query.split('=')[1]
                const user = await middleware.decodeJWT(token)
                if (!user) {
                    ws.send(JSON.stringify({
                        error: 'Unauthorization !'
                    }))
                } else {
                    ws.room = await RoomUser.findAll({
                        attributes: ['RoomId'],
                        where: {
                            UserId: user.id
                        }
                    })
                    console.log(user.id + 'client connect')

                    this.messageComing(ws, user)
                }
            } catch (err) {
                console.log(err.message)
            }
        })
    }

    messageComing(ws, user) {
        ws.on('message', async (payload) => {
            try {
                let mess = JSON.parse(payload)
                const checkRoom = await RoomService.checkRoomExist(mess.data.RoomId)

                switch (mess.type) {
                    case 'join':
                        if (checkRoom) {
                            const historyMess = await Message.findAll({
                                where: {
                                    RoomId: mess.data.RoomId
                                }
                            })
                            for (let item of historyMess) {
                                mess.data = item
                                ws.send(JSON.stringify(mess))
                            }
                        } else {
                            ws.send(JSON.stringify({error: 'Room not found!'}))
                        }
                        break
                    case 'send':
                        if(checkRoom){
                            let model = mess.data
                            model.UserId = user.id
                            await Promise.all([
                                this.broadcast(mess),
                                Message.create(model)
                            ])
                        }else{
                            ws.send(JSON.stringify({error: 'Room not found!'}))
                        }
                        break
                }
            } catch (err) {
                console.log(err)
            }
        })

    }
    // van chuyen mess
    broadcast(mess) {
        return new Promise(() => {
            this.wss.clients.forEach(client => {
                const findRoom = client.room.find(item => item.RoomId == mess.data.RoomId ? true : false)
                if (findRoom) {
                    client.send(JSON.stringify(mess))
                }
            })
        })
    }
}
module.exports = ChatBroker