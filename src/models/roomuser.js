const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db/driver');

class RoomUser extends Model {}
RoomUser.init({
    UserId : {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    RoomId : {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
},{
    sequelize: sequelize ,
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    modelName: 'RoomUser',
    timestamps :false,
    freezeTableName :true
})
module.exports = RoomUser