const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db/driver');
class Message extends Model {}
Message.init({
    id : {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    UserId: {type : DataTypes.INTEGER},
    RoomId: {type : DataTypes.INTEGER},
    content: {type: DataTypes.STRING(2048)}
},{
    sequelize: sequelize ,
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    modelName: 'Message',
    timestamps :true,
    freezeTableName :true
})
module.exports = Message