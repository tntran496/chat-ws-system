const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db/driver');
const Message = require('./message');
const RoomUser = require('./roomuser');

class Room extends Model {}
Room.init({
    id : {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name : {type: DataTypes.STRING},
    description : {type: DataTypes.STRING},
    image : {type: DataTypes.STRING},
    UserId : {type : DataTypes.INTEGER}
},{
    sequelize: sequelize ,
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    modelName: 'Room',
    timestamps :true,
    freezeTableName :true
})
Room.hasMany(Message,{foreignKey:'RoomId'})
Message.belongsTo(Room)

Room.hasMany(RoomUser,{foreignKey:'RoomId'})
RoomUser.belongsTo(Room)

module.exports = Room
