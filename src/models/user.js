const { DataTypes, Model } = require('sequelize');
const sequelize = require('../config/db/driver');
const Room = require('./room')
const Message = require('./message')
const RoomUser = require('./roomuser')

class User extends Model {}
User.init({
    id : {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    fullName : {type: DataTypes.STRING},
    email : {type: DataTypes.STRING},
    picture : {type: DataTypes.STRING},
},{
    sequelize: sequelize ,
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    modelName: 'User',
    timestamps :true,
    freezeTableName :true
})

User.hasMany(Room,{foreignKey : 'UserId'})
Room.belongsTo(User)

User.hasMany(Message,{foreignKey : 'UserId'})
Message.belongsTo(User)

User.hasMany(RoomUser,{foreignKey : 'UserId'})
RoomUser.belongsTo(User)

module.exports = User
