function errorHandler (req, res, next,err) {
    if (res.headersSent) {
      return next(err)
    }
    res.status(500).send({ error: err.message })
}

module.exports = errorHandler