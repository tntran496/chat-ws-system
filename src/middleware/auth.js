const jwt = require('jsonwebtoken')
const hanErr = require('../utils/error')
const User = require('../models/user')
const myKey = process.env.KEY_PRIVATE

let middleware = {
    auth: async (req, res, next) => {
        try {
            let token = req.header('Authorization').trim().split(' ')
            console.log(token[2])
            const data = await jwt.verify(token[2], myKey)
            const existUser = await User.findOne({
                where: {
                    id: data.id
                }
            })
            console.log(data,existUser)
            if (!existUser) {
                return res.status(403).send({
                    error: 'Unauthorized'
                })
            }
            req.user = existUser
            req.token = token
            next()
        } catch (error) {
            hanErr(req, res, next, error)
        }
    },

    generateJWT: async (user) => {
        return await jwt.sign(
            user, 
            myKey, 
            {expiresIn: '1h'}
        )
    },

    decodeJWT: async (token) => {
        return await jwt.verify(token, myKey)
    },

}

module.exports = middleware