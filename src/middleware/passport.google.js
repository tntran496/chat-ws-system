const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const User = require('../models/user');

const googleConfig = {
    clientID: "724565456457-npooqitq0e6biia0ll61mj1ermdjgn3v.apps.googleusercontent.com",
    clientSecret: "ib93-W5RP59peMhAmmAhutHi",
    callbackURL: "/auth/google/redirect"
}

const responseAuthen = async (accessToken, refreshToken, profile, done) => {
    //passport callback function
    //check if user already exists in our db with the given profile ID
    const existUser = await User.findOne({
        where: {
            id: profile.id
        }
    })
    if (existUser) {
        done(null, existUser.id);
    } else {
        const {
            _json
        } = profile
        const newUser = await User.create({
            id: _json.sub,
            fullName: _json.family_name,
            email: _json.email,
            picture: _json.picture,
        })
        done(null, newUser.id)
    }
}

passport.use(new GoogleStrategy(googleConfig,responseAuthen))

module.exports = passport